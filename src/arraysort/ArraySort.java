package arraysort;
import java.util.Scanner;
import java.io.*;
import java.util.Arrays;
/**
 *
 * @author Jovon
 */
public class ArraySort {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
    System.out.println("Input 10 numbers. Press 'Enter' to confirm each input.");
    Scanner keyboard = new Scanner(System.in);
    int[] array = new int[10];
    
    for ( int i = 0 ; i < array.length ; i++ ) 
    {
            array[i] = keyboard.nextInt();
    }
    
    System.out.println("The sorted array looks like this:");
    Arrays.sort(array);
    System.out.println(Arrays.toString(array));
    
  }
    
    
}